<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-main">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-2 position-relative header-logo-col">
					<?php if ($logo = opt('logo')) : ?>
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
					<?php endif; ?>
				</div>
				<div class="col d-flex align-items-center menu-col">
					<nav id="MainNav">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
					</nav>
					<div class="wrap-search">
						<?= get_search_form() ?>
					</div>
				</div>
				<?php if ($tel = opt('tel')) : ?>
					<div class="col-auto d-flex align-items-center justify-content-end tel-head-col">
						<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
							<img src="<?= ICONS ?>header-tel.png" class="header-tel-icon">
							<span class="base-title tel-text"><?= $tel; ?></span>
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
<div class="triggers-wrap">
	<?php if ($all_places = opt('to_places_link')) : ?>
	<a class="menu-trigger" href="<?= $all_places['url']; ?>">
		<?php if ($logo_white = opt('logo_white')) : ?>
			<img src="<?= $logo_white['url']; ?>" alt="all-places">
		<?php endif; ?>
		<span class="side-title">
			<?= isset($all_places['title']) ? $all_places['title'] : 'מעבר לכל
בתי האבות'; ?>
		</span>
	</a>
	<?php endif; ?>
	<div class="pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="open-popup">
		<span class="pop-text-trigger">צרו קשר</span>
	</div>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="form-col pb-4">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
						<?php if ($logo_white) : ?>
							<a class="logo-form" href="/">
								<img src="<?= $logo_white['url']; ?>" alt="logo">
							</a>
						<?php endif;
						if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="form-title text-center"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('pop_form_text')) : ?>
							<h3 class="form-subtitle text-center"><?= $f_text; ?></h3>
						<?php endif;
						getForm('7'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
