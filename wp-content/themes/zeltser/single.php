<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
switch ($currentType) {
	case 'place':
		$taxname = 'location';
		$sameText = '<h2>בתי אבות בסגנון</h2>';
		break;
	case 'post':
		$taxname = 'category';
		$sameText = '<h2>מאמרים נוספים</h2>';
		break;
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$dep_query = [];
$type_query = [];
$cats = [];
if ($currentType === 'place') {
	$post_terms_dep = wp_get_object_terms($postId, 'department', ['fields' => 'ids']);
	$post_terms_type = wp_get_object_terms($postId, 'place_type', ['fields' => 'ids']);
	$dep_query = [
			'taxonomy' => 'department',
			'field' => 'term_id',
			'terms' => $post_terms_dep,
	];
	$type_query = [
			'taxonomy' => 'place_type',
			'field' => 'term_id',
			'terms' => $post_terms_type,
	];
	$cats = [$post_terms, $post_terms_dep, $post_terms_type];
}
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
			$dep_query,
			$type_query,
	],
]);
if (isset($fields['same_homes']) && $fields['same_homes'] && ($currentType === 'place')) {
	$samePosts = $fields['same_homes'];
} elseif (isset($fields['same_posts']) && $fields['same_posts'] && ($currentType === 'post')) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
?>
<article class="post-body">
	<div class="container">
		<div class="row justify-content-center align-items-start">
			<div class="col-lg-6 col-12">
				<h1 class="base-title"><?php the_title(); ?></h1>
				<?php if ($currentType === 'place') : if ($cats) : ?>
					<div class="cats-wrap">
						<?php foreach ($cats as $post_terms_all) :
							foreach ($post_terms_all as $cat) : $term = get_term($cat); ?>
							<a class="post-subtitle" href="<?= get_category_link($cat); ?>">
								<?= $term->name; ?>
							</a>
						<?php endforeach;
						endforeach; ?>
 					</div>
					<?php endif; if ($fields['home_tel']) : ?>
						<a href="tel:<?= $fields['home_tel']; ?>" class="base-title home-tel">
							טלפון: <?= $fields['home_tel']; ?>
						</a>
					<?php endif;
					if ($fields['home_desc']) : ?>
						<div class="post-text-output">
							<p class="mb-2"><?= $fields['home_desc']; ?></p>
						</div>
					<?php endif; ?>
					<div class="d-flex flex-column align-items-start home-info-block">
						<?php if ($fields['home_address']) : ?>
							<a href="https://waze.com/ul?q=<?= $fields['home_address']; ?>" target="_blank" class="post-subtitle">
								<?= $fields['home_address']; ?>
							</a>
						<?php endif;
						if ($fields['home_mail']) : ?>
							<a href="mailto:<?= $fields['home_mail']; ?>" target="_blank" class="post-subtitle">
								מייל: <?= $fields['home_mail']; ?>
							</a>
						<?php endif;
						if ($fields['home_site']) : ?>
							<a href="<?= $fields['home_site']; ?>" target="_blank" class="post-subtitle">
								אתר: <?= $fields['home_site']; ?>
							</a>
						<?php endif; ?>
					</div>
					<?php if (has_post_thumbnail()) : ?>
						<img src="<?= postThumb(); ?>" alt="home-img" class="w-100">
					<?php endif;
					elseif ($currentType === 'post') : ?>
						<h2 class="post-subtitle"><?php the_author(); ?></h2>
						<div class="post-text-output"><?= $fields['post_content']; ?></div>
						<?php if (has_post_thumbnail()) : ?>
							<img src="<?= postThumb(); ?>" alt="home-img" class="w-100">
						<?php endif;
						endif; ?>
			</div>
			<div class="col-lg-6 col-12">
				<div class="post-form-wrap <?= ($currentType === 'place') ? 'place-form-wrap' : ''; ?>">
					<?php get_template_part('views/partials/repeat', 'form_post', [
						'logo' => ($currentType === 'place') ? opt('logo_white') : opt('symbol'),
					]); ?>
				</div>
				<?php if (($currentType === 'post') && $fields['post_adv_img']) : ?>
					<img src="<?= $fields['post_adv_img']['url']; ?>" class="w-100">
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>

<?php if (($currentType === 'place') && $fields['home_video']) : ?>
<section class="video-post">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="post-video-item" style="background-image: url('<?= getYoutubeThumb($fields['home_video']); ?>')"
				data-video="<?= getYoutubeId($fields['home_video']); ?>">
					<div class="put-video-here"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
if ($fields['adv_img_1']) : ?>
	<section class="banner-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<img src="<?= $fields['adv_img_1']['url']; ?>">
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<section class="content-gallery-block arrows-slider gallery-arrows-slider">
	<div class="container">
		<div class="row justify-content-center">
			<?php if (($currentType === 'post') && $fields['post_content']) : ?>
				<div class="col-lg-6 col-12">
					<div class="post-text-output"><?= $fields['post_content']; ?></div>
				</div>
			<?php else: ?>
				<div class="col-lg-6 col-12">
					<div class="post-text-output"><?php the_content(); ?></div>
				</div>
			<?php endif;
			if ($fields['post_gallery'] || (isset($fields['home_map_iframe']) && $fields['home_map_iframe'])) : ?>
				<div class="col-lg-6 col-12 gallery-slider-col">
					<?php if (isset($fields['home_map_iframe']) && $fields['home_map_iframe']) : ?>
						<div class="iframe-map">
							<?= $fields['home_map_iframe']; ?>
						</div>
					<?php endif;
					if ($fields['post_gallery']) : ?>
					<div class="gallery-slider" dir="rtl">
						<?php foreach ($fields['post_gallery'] as $img) : ?>
							<div class="slide-gallery">
								<a class="gallery-img" style="background-image: url('<?= $img['url']; ?>')"
								href="<?= $img['url']; ?>" data-lightbox="gallery"></a>
							</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php if (($currentType === 'place') && $fields['adv_img_2']) {
	get_template_part('views/partials/repeat', 'banner_fluid', [
			'banner' => $fields['adv_img_2'],
	]);
}
if ($samePosts) : ?>
	<section class="same-posts-block">
		<div class="container-fluid">
			<?php get_template_part('views/partials/content', 'block_text', [
					'text' => $sameText,
			]); ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $x => $post) : ?>
					<div class="col-lg-3 col-sm-6 col-12 post-col">
						<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
								'post_type' => $currentType,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if (($currentType === 'post') && $fields['adv_img_2']) {
	get_template_part('views/partials/repeat', 'banner_fluid', [
			'banner' => $fields['adv_img_2'],
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
get_footer(); ?>
