(function($) {
	//Select
	$('#inputLoc').change(function() {
		var location = '';
		$( '#inputLoc option:selected').each(function() {
			location = $( this ).data('id');
		});
		var select = $('.form-group').find($('#inputCity'));
		$('#inputCity').children('option:not(:first)').remove();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				location: location,
				action: 'models_search',
			},
			success: function (data) {

				var options = data.html;
				$.each(options, function (i, item) {
					select.append($('<option>', {
						value: i,
						text : item
					}));
				});
			}
		});
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		// $('.menu-item-has-children').hover(
		// 	function() {
		// 		var menuSub = $( this ).children('.sub-menu');
		// 		var subCont = $('.sub-container');
		// 		subCont.append(menuSub);
		// 		subCont.addClass('show-sub');
		// 	}, function() {
		// 		var subCont = $('.sub-container').html();
		// 		subCont.removeClass('show-sub');
		// 	}
		// );
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.post-video-item').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + id + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').html(frame);
		});
		// $('.play-button').click(function() {
		// 	var id = $(this).data('video');
		// 	var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
		// 	$('#iframe-wrapper').html(frame);
		// 	$('#modalCenter').modal('show');
		// });
		// $('#modalCenter').on('hidden.bs.modal', function (e) {
		// 	$('#iframe-wrapper').html('');
		// });
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});
	});
	//More posts
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
