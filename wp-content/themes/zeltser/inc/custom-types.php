<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות מוצרים',
            'singular_name'              => 'קטגוריות מוצרים',
            'menu_name'                  => 'קטגוריות מוצרים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}

if(PLACES){
	function place_post_type() {

		$labels = array(
			'name'                => 'בתי אבות',
			'singular_name'       => 'בית אבות',
			'menu_name'           => 'בתי אבות',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל הבתי אבות',
			'view_item'           => 'הצג בית אבות',
			'add_new_item'        => 'הוסף בית אבות חדש',
			'add_new'             => 'הוסף חדש',
			'edit_item'           => 'ערוך  בית אבות',
			'update_item'         => 'עדכון  בית אבות',
			'search_items'        => 'חפש  בית אבות',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'place',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'place',
			'description'         => ' בתי אבות',
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies'          => array( 'department', 'location', 'place_type' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-bank',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'place', $args );

	}

	add_action( 'init', 'place_post_type', 0 );

	function department_taxonomy() {

		$labels = array(
			'name'                       => 'מחלקות',
			'singular_name'              => 'מחלקה',
			'menu_name'                  => 'מחלקות',
			'all_items'                  => 'כל המחלקות',
			'parent_item'                => 'מחלקת הורה',
			'parent_item_colon'          => 'מחלקת הורה:',
			'new_item_name'              => 'שם מחלקה חדשה',
			'add_new_item'               => 'להוסיף מחלקה חדשה',
			'edit_item'                  => 'ערוך מחלקה',
			'update_item'                => 'עדכן מחלקה',
			'separate_items_with_commas' => 'מחלקות נפרדות עם פסיק',
			'search_items'               => 'חיפוש מחלקות',
			'add_or_remove_items'        => 'להוסיף או להסיר מחלקות',
			'choose_from_most_used'      => 'בחר מהמחלקות הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'department',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'department', array( 'place' ), $args );
	}

	add_action( 'init', 'department_taxonomy', 0 );

	function location_taxonomy() {

		$labels = array(
			'name'                       => 'אזורים',
			'singular_name'              => 'אזור',
			'menu_name'                  => 'אזורים',
			'all_items'                  => 'כל האזורים',
			'parent_item'                => 'אזור הורה',
			'parent_item_colon'          => 'אזור הורה:',
			'new_item_name'              => 'שם אזור חדש',
			'add_new_item'               => 'להוסיף אזור חדש',
			'edit_item'                  => 'ערוך אזור',
			'update_item'                => 'עדכן אזור',
			'separate_items_with_commas' => 'אזורים נפרדות עם פסיק',
			'search_items'               => 'חיפוש אזורים',
			'add_or_remove_items'        => 'להוסיף או להסיר אזורים',
			'choose_from_most_used'      => 'בחר מהאזורים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'location',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'location', array( 'place' ), $args );
	}

	add_action( 'init', 'location_taxonomy', 0 );

	function place_type_taxonomy() {

		$labels = array(
			'name'                       => 'סוגי מוסדיים',
			'singular_name'              => 'סוג מוסד',
			'menu_name'                  => 'סוגי מוסדיים',
			'all_items'                  => 'כל הסוגי מוסדיים',
			'parent_item'                => 'סוג מוסד הורה',
			'parent_item_colon'          => 'סוג מוסד הורה:',
			'new_item_name'              => 'שם סוג מוסד חדש',
			'add_new_item'               => 'להוסיף סוג מוסד חדש',
			'edit_item'                  => 'ערוך סוג מוסד',
			'update_item'                => 'עדכן סוג מוסד',
			'separate_items_with_commas' => 'סוגי מוסדיים נפרדות עם פסיק',
			'search_items'               => 'חיפוש סוגי מוסדיים',
			'add_or_remove_items'        => 'להוסיף או להסיר סוגי מוסדיים',
			'choose_from_most_used'      => 'בחר מהסוגי מוסדיים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'place_type',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'place_type', array( 'place' ), $args );
	}

	add_action( 'init', 'place_type_taxonomy', 0 );
}
