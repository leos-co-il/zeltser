<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_models_search', 'models_search');
add_action('wp_ajax_models_search', 'models_search');


function models_search()
{

	$result['type'] = "success";

	$term_id = (isset($_REQUEST['location'])) ? $_REQUEST['location'] : '';
	$terms = get_term_children( $term_id, 'location');
	$html = [];
	foreach ($terms as $term) {
		$term_name = get_term($term)->name;
		$html[$term] = $term_name;
//		$html = load_template_part('views/partials/card', 'option', [
//			'item' => $term,
//		]);
	}
	$result['html'] = $html;

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$post_type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : '';
	$tax_name = (isset($_REQUEST['taxType'])) ? $_REQUEST['taxType'] : 'category';
	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
	$hi = json_decode($params_string, true, 4);
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $post_type ? $post_type : 'post',
		'posts_per_page' => 4,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => $tax_name,
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	if ($post_type === 'post') {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post_ajax', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	} else {
		$hi['post__not_in'] = $ids;
		$hi['posts_per_page'] = 4;
		$hi['post_type'] = 'place';
		$result['html'] = '';
		$query = new WP_Query($hi);
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'search', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
