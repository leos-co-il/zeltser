<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<article class="page-block">
	<?php if (has_post_thumbnail()) : ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<img src="<?= postThumb(); ?>" alt="home-img" class="w-100">
			</div>
		</div>
	</div>
	<?php endif;
	get_template_part('views/partials/repeat', 'search');
	if ($fields['adv_img_1']) {
		get_template_part('views/partials/repeat', 'banner_fluid', [
				'banner' => $fields['adv_img_1'],
		]);
	}
	if ($fields['h_homes_chosen'] || $fields['h_homes_text'] || $fields['h_homes_link']) : ?>
		<div class="departments-block">
			<div class="container-fluid">
				<?php if ($fields['h_homes_text']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['h_homes_text'],
					]);
				}
				if ($fields['h_homes_chosen']) : ?>
					<div class="row justify-content-center align-items-stretch mt-2">
						<?php foreach ($fields['h_homes_chosen'] as $y => $home) : ?>
							<div class="col-lg-4 col-12 wow zoomIn cat-col home-cat-col"
								 data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'post_home', [
										'post' => $home,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if ($fields['h_homes_link']) : ?>
					<div class="row justify-content-end my-2">
						<div class="col-auto">
							<a href="<?= $fields['h_homes_link']['url']; ?>" class="base-link">
								<?= isset($fields['h_homes_link']['title']) ? $fields['h_homes_link']['title'] : 'לכל המומלצים'; ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;
	if ($fields['adv_img_2']) {
		get_template_part('views/partials/repeat', 'banner_fluid', [
				'banner' => $fields['adv_img_2'],
		]);
	}
	if ($fields['h_locations_chosen'] || $fields['h_locations_text']) : ?>
		<section class="locations-block">
			<div class="container-fluid">
				<?php if ($fields['h_locations_text']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['h_locations_text'],
					]);
				}
				if ($fields['h_locations_chosen']) : ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['h_locations_chosen'] as $y => $location) : ?>
							<div class="col-lg-3 col-sm-6 col-12 wow zoomIn cat-col" data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'category_location', [
										'category' => $location,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>
	<?php endif;
	get_template_part('views/partials/repeat', 'form');
	if ($fields['h_slider_content']) {
		get_template_part('views/partials/content', 'slider', [
				'content' => $fields['h_slider_content'],
				'img' => $fields['h_slider_img'],
		]);
	}
	if ($fields['h_departments_chosen'] || $fields['h_departments_text']) : ?>
		<div class="departments-block">
			<div class="container-fluid">
				<?php if ($fields['h_departments_text']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['h_departments_text'],
					]);
				}
				if ($fields['h_departments_chosen']) : ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['h_departments_chosen'] as $y => $cat_dep) : ?>
							<div class="col-lg-3 col-sm-6 col-12 wow zoomIn cat-col" data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'category_department', [
										'category' => $cat_dep,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;
	if ($fields['h_adv_img'] || $fields['h_posts_block']) : ?>
		<section class="repeat-post-block">
			<div class="container-fluid">
				<div class="row justify-content-center align-items-stretch">
					<?php if ($fields['h_adv_img']) : ?>
						<div class="col-lg-6">
							<img src="<?= $fields['h_adv_img']['url']; ?>" class="w-100 mb-3">
						</div>
					<?php endif;
					if ($fields['h_posts_block']) {
						foreach ($fields['h_posts_block'] as $post_block) {
							get_template_part('views/partials/content', 'post_block', [
									'block' => $post_block,
							]);
						}
					} ?>
				</div>
			</div>
		</section>
	<?php endif;
	if ($fields['h_homes_chosen_more'] || $fields['h_homes_text_more'] || $fields['h_homes_link_more']) : ?>
		<div class="departments-block">
			<div class="container-fluid">
				<?php if ($fields['h_homes_text_more']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['h_homes_text_more'],
					]);
				}
				if ($fields['h_homes_chosen_more']) : ?>
					<div class="row justify-content-center align-items-stretch mt-2">
						<?php foreach ($fields['h_homes_chosen_more'] as $y => $home) : ?>
							<div class="col-lg-4 col-12 wow zoomIn cat-col home-cat-col"
								 data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'post_home', [
										'post' => $home,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if ($fields['h_homes_link_more']) : ?>
					<div class="row justify-content-end my-2">
						<div class="col-auto">
							<a href="<?= $fields['h_homes_link_more']['url']; ?>" class="base-link">
								<?= isset($fields['h_homes_link_more']['title']) ? $fields['h_homes_link_more']['title'] : 'לכל המומלצים'; ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
get_footer(); ?>
