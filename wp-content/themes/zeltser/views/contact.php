<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
?>

<article class="contact-page pt-4">
	<div class="container mb-3">
		<?php if ($logo = opt('logo')) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<a class="logo-contact" href="/">
						<img src="<?= $logo['url']; ?>" alt="logo">
					</a>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center align-items-stretch">
			<div class="col-lg-6 col-sm-12 col-11 contact-col-back" <?php if (has_post_thumbnail()) : ?>
				style="background-image: url('<?= postThumb(); ?>')"
			<?php endif; ?>>
				<div class="row contact-green-row justify-content-around">
					<?php if ($tel) : ?>
						<div class="col-xl-5 col-lg-6 col-sm-5 col-12">
							<a href="tel:<?= $tel; ?>" class="contact-d-col">
								<span class="base-text mb-1"> התקשרו אלינו</span>
								<span class="base-title"><?= $tel; ?></span>
							</a>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="col-xl-5 col-lg-6 col-sm-5 col-12">
							<a href="tel:<?= $mail; ?>" class="contact-d-col">
								<span class="base-text mb-1"> כתבו לנו</span>
								<span class="base-title"><?= $mail; ?></span>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-lg-6 col-sm-12 col-11 contact-form-col">
				<div class="form-col">
					<?php if ($fields['contact_form_title']) : ?>
						<h2 class="form-title text-center"><?= $fields['contact_form_title']; ?></h2>
					<?php endif;
					if ($fields['contact_form_text']) : ?>
						<h3 class="form-subtitle text-center mb-2"><?= $fields['contact_form_text']; ?></h3>
					<?php endif;
					getForm('7'); ?>
				</div>
			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
