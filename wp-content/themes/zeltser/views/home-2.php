<?php
/*
Template Name: דף הבית 2
*/

get_header();
$fields = get_fields();

?>

<article class="page-block">
	<div class="container-fluid">
		<div class="row justify-content-center align-items-start">
			<?php if ($fields['home_news'] || $fields['home_main_post']) : ?>
				<div class="col-xl-6 col-12">
					<div class="row justify-content-center align-items-start">
						<?php if ($fields['home_news']) : ?>
							<div class="col-md-5 col-sm-6 col-news">
								<?php foreach ($fields['home_news'] as $num => $news) : ?>
									<a class="news-line wow fadeInUp" data-wow-delay="0.<?= $num; ?>s"
									href="<?php the_permalink($news); ?>">
										<div class="d-flex flex-column justify-content-start align-items-start">
											<h3 class="post-info">
												<?= get_the_date('d|m|y H:i',$news); ?>
											</h3>
											<p class="post-preview-small">
												<?= text_preview($news->post_content, 15); ?>
											</p>
										</div>
									</a>
								<?php endforeach; ?>
							</div>
						<?php endif;
						if ($fields['home_main_post'] && isset($fields['home_main_post']['0'])) : ?>
							<div class="col-md-7 col-sm-6">
								<div class="post-col">
									<h3 class="date-preview"><?= get_the_date('d|m|y H:i',$fields['home_main_post']['0']); ?></h3>
									<h3 class="base-title"><?= $fields['home_main_post']['0']->post_title; ?></h3>
									<h4 class="post-subtitle"><?php $author_id = $fields['home_main_post']['0']->post_author;
										echo get_the_author_meta('display_name', $author_id); ?></h4>
									<p class="post-item-title">
										<?= text_preview($fields['home_main_post']['0']->post_content, 20)?>
									</p>
									<a href="<?php the_permalink($fields['home_main_post']['0']); ?>" class="base-link">
										להמשך קריאה
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif;
			if (has_post_thumbnail()) : ?>
				<div class="col-xl-6 col-12 img-col-main">
					<img src="<?= postThumb(); ?>" alt="home-img" class="w-100">
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'form');
	if ($fields['home_link_block'] || $fields['home_posts_block']) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch">
				<?php if ($fields['home_link_block']) : ?>
					<div class="col-xl-4 col-lg-5 col-12 col-link-img-wrap">
						<div class="col-link-img" <?php if ($fields['home_link_block']['link_block_img']) : ?>
							style="background-image: url('<?= $fields['home_link_block']['link_block_img']['url']; ?>')"
						<?php endif; ?>>
							<?php if ($logo = opt('logo')) : ?>
								<a href="/" class="logo-link">
									<img src="<?= $logo['url'] ?>" alt="logo">
								</a>
							<?php endif;
							if ($fields['home_link_block']['link_block_title']) : ?>
								<h3 class="cat-item-title"><?= $fields['home_link_block']['link_block_title']; ?></h3>
							<?php endif;
							if ($fields['home_link_block']['link_block_text']) : ?>
								<p class="base-text-20"><?= $fields['home_link_block']['link_block_text']; ?></p>
							<?php endif;
							if ($fields['home_link_block']['link_block_link']) : ?>
								<a href="<?= $fields['home_link_block']['link_block_link']['url']; ?>" class="home-link">
									<?= isset($fields['home_link_block']['link_block_link']['title']) ?
									$fields['home_link_block']['link_block_link']['title'] : 'להמשך קריאה'; ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endif;
				if ($fields['home_posts_block']) : ?>
					<div class="col-xl-8 col-lg-7 col-12">
						<?php foreach ($fields['home_posts_block'] as $k => $post) : $link = get_the_permalink($post); ?>
							<div class="home-post-item-wrap wow fadeInUp" data-wow-delay="0.<?= $k; ?>s">
								<div class="post-item-img home-post-item-img" <?php if (has_post_thumbnail($post)) : ?>
									 style="background-image: url('<?= postThumb($post); ?>')"<?php endif; ?>></div>
								<div class="home-post-item">
									<h3 class="post-info font-weight-bold">
										<?= get_the_date('d|m|y H:i', $post); ?>
									</h3>
									<a class="base-text font-weight-bold" href="<?= $link; ?>">
										<?= $post->post_title; ?>
									</a>
									<h3 class="date-preview font-weight-normal">
										<?php $author_id = $post->post_author;
										echo get_the_author_meta('display_name', $author_id); ?>
									</h3>
									<p class="base-text mb-3">
										<?= text_preview($post->post_content, 25); ?>
									</p>
									<a href="<?= $link; ?>" class="base-link">
										למאמר
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;
	if ($fields['home_slider_content']) {
		get_template_part('views/partials/content', 'slider', [
			'content' => $fields['home_slider_content'],
			'img' => $fields['home_slider_img'],
		]);
	}
	if ($fields['home_posts_block_chosen']) : ?>
		<section class="repeat-post-block">
			<div class="container-fluid">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['home_posts_block_chosen'] as $post_block) {
						get_template_part('views/partials/content', 'post_block', [
							'block' => $post_block,
						]);
					} ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
get_footer(); ?>
