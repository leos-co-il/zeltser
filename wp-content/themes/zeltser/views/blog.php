<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$posts = get_posts([
	'numberposts' => 10,
	'post_type' => 'post',
]);
$posts_all = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
]);
?>
<article class="page-body">
	<div class="container-fluid">
		<?php get_template_part('views/partials/content', 'block_text', [
				'text' => get_the_content(),
		]); ?>
	</div>
	<div class="body-output">
		<?php if ($posts) : $counter = 1; ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($posts as $x => $post) : ?>
				<div class="col-xl-3 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
					<?php get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]); ?>
				</div>
				<?php if(($counter === 2) && $fields['adv_img_1']) : ?>
					<div class="col-xl-6 col-12 mb-3">
						<img src="<?= $fields['adv_img_1']['url']; ?>" class="w-100">
					</div>
				<?php endif; ?>
				<?php if(($counter % 6 === 0) || ($counter === count($posts) && $counter <= 6)) : ?>
			</div>
		</div>
		<?php if ($fields['adv_img_2']) {
			get_template_part('views/partials/repeat', 'banner_fluid', [
					'banner' => $fields['adv_img_2'],
			]);
		} ?>
		<div class="container-fluid pt-4">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php endif; $counter++; endforeach; ?>
			</div>
		</div>
	</div>
<?php endif;
if (count($posts_all) > 10) : ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="load-more-link load-more-posts" data-type="post" data-tax-type="category">הצג מאמרים נוספים</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

