<?php
/*
Template Name: בתי אבות
*/
get_header();
$fields = get_fields();
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'place',
]);
$_place_locations = (isset($_GET['place-location'])) ? $_GET['place-location'] : null;
$_place_cities = (isset($_GET['place-city'])) ? $_GET['place-city'] : null;
$_place_types = (isset($_GET['place-type'])) ? $_GET['place-type'] : null;
$_departments = (isset($_GET['place-department'])) ? $_GET['place-department'] : null;
$_query = (isset($_GET['search-query'])) ? sanitize_text_field($_GET['search-query']) : null;
$props = ['prop_1', 'prop_2', 'prop_3', 'prop_4', 'prop_5'];
$properties_check_all = [];
foreach ($props as $prop) {
	$properties_check['key'] = (isset($_GET[$prop]) && !empty($_GET[$prop])) ? $_GET[$prop] : null;
	if ($properties_check['key']) {
		$properties_check['value'] = (isset($_GET[$prop]) && !empty($_GET[$prop])) ? true : null;
		$properties_check['compare'] = 'LIKE';
		$properties_check_all[] = $properties_check;
	}
}
$query_args = [
		'post_type' => 'place',
		'posts_per_page' => 4,
		'meta_query' => $properties_check_all,
];
if($_place_locations || $_place_cities || $_place_types || $_departments){
	$query_args['tax_query'] = [
			'relation' => 'AND',
			$_place_locations ? [
					'taxonomy' => 'location',
					'field'    => 'term_id',
					'terms'    => [$_place_locations],
			] : null,
			$_place_cities ? [
					'taxonomy' => 'location',
					'field'    => 'term_id',
					'terms'    => [$_place_cities],
			] : null,
			$_place_types ? [
					'taxonomy' => 'place_type',
					'field'    => 'term_id',
					'terms'    => $_place_types,
			] : null,
			$_departments ? [
					'taxonomy' => 'department',
					'field' => 'term_id',
					'terms' => [$_departments],
			] : null,
	];
}else{
	$query_args['tax_query'] = null;
}

if($_query){
	$query_args['s'] = $_query;
}
$posts = new WP_Query($query_args);
$json = json_encode($query_args);
$cats_all = [
		[
				'title' => 'אוזר',
				'objects' => get_terms([
						'taxonomy' => 'location',
						'hide_empty' => false,
						'parent' => 0,
				]),
		],
		[
				'title' => 'סוג מוסד',
				'objects' => get_terms([
						'taxonomy' => 'place_type',
						'hide_empty' => false,
						'parent' => 0,
				]),
		],
		[
				'title' => 'מחלקה',
				'objects' => get_terms([
						'taxonomy' => 'department',
						'hide_empty' => false,
						'parent' => 0,
				]),
		]
]
?>
<article class="page-body py-2">
	<?php get_template_part('views/partials/repeat', 'form', [
			'search_title' => opt('mid_form_title_search'),
	]); ?>
	<div class="body-output">
		<div class="container-fluid">
			<?php if ($fields['search_page_title'] || $fields['search_page_subtitle']) : ?>
				<div class="row justify-content-center align-items-center mb-2">
					<?php if ($fields['search_page_title']) : ?>
						<div class="col-auto">
							<h2 class="search-title"><?= $fields['search_page_title']; ?></h2>
						</div>
					<?php endif;
					if ($fields['search_page_subtitle']) : ?>
						<div class="col-auto">
							<h2 class="base-title font-weight-normal"><?= $fields['search_page_subtitle']; ?></h2>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-3 col-md-4 col-12 sticky-form">
					<?php if ($cats_all) : foreach ($cats_all as $cat_block) : ?>
						<div class="cat-block-search">
							<div class="search-cat-title"><?= $cat_block['title']; ?></div>
							<ul class="search-cats-list">
								<?php foreach ($cat_block['objects'] as $term) : ?>
									<li class="cat-item-list">
										<a href="<?= get_term_link($term); ?>" class="base-text">
											<?= $term->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endforeach; endif; ?>
					<div class="cat-block-search">
						<div class="search-cat-title mb-3">סינון לפי</div>
						<form action="<?php the_permalink(); ?>" method="get" name="search">
							<?php foreach ($props as $item) : $key = get_field_object($item, $posts_all['0']); ?>
								<div class="checkbox">
									<input type="checkbox" id="<?= $item; ?>"
										   name="<?= $item; ?>"
										   value="<?= $item; ?>">
									<label for="<?= $item; ?>"><?= $key['label']; ?></label>
								</div>
							<?php endforeach; ?>
							<input type="submit" value="החל סינון" id="search-products" class="search-place">
						</form>
					</div>
				</div>
				<div class="col-xl-7 col-md-8 col-12">
					<div class="row">
						<div class="border-padding col-12"></div>
					</div>
					<?php if ($posts->have_posts()) : ?>
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($posts->posts as $x => $post) : ?>
								<?php get_template_part('views/partials/card', 'search', [
										'post' => $post,
								]); ?>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<div class="row">
							<div class="col-12">
								<h3 class="base-title text-center">
									<?= esc_html__('שום דבר לא נמצא','leos'); ?>
								</h3>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (count($posts_all) > 4 && $posts->have_posts()) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-link load-more-posts" data-type="place">הצג מאמרים נוספים</div>
					<span class="take-json d-none"><?= $json; ?></span>
				</div>
			</div>
		</div>
	<?php endif;
	if ($fields['adv_img_1']) {
		get_template_part('views/partials/repeat', 'banner_fluid', [
			'banner' => $fields['adv_img_1'],
		]);
	} ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
get_footer(); ?>

