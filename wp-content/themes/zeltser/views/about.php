<?php
/*
Template Name: אודות
*/
get_header();
$fields = get_fields();
?>
<article class="page-body about-page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-11">
				<div class="base-output about-text-output">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
			<?php if(has_post_thumbnail()) : ?>
				<div class="col-12">
					<img src="<?= postThumb(); ?>" alt="about-us" class="w-100 my-4">
				</div>
			<?php endif;
			if ($fields['about_content_team']) : ?>
				<div class="col-11">
					<div class="base-output about-text-output">
						<?= $fields['about_content_team']; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<div class="simple-repeat-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php get_footer(); ?>

