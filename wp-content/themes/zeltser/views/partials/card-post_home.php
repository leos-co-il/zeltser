<?php if(isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-item post-home-item">
		<a class="post-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
		   href="<?= $link; ?>"></a>
		<div class="post-item-content">
			<div class="d-flex align-items-center flex-wrap">
				<h3 class="post-item-title ml-3"><?= $args['post']->post_title; ?></h3>
				<?php if ($post_terms_loc = wp_get_object_terms($args['post']->ID, 'location', ['fields' => 'ids'])) : ?>
					<div class="cats-wrap locs-wrap">
						<?php foreach ($post_terms_loc as $cat) : $term = get_term($cat); ?>
							<a class="post-info" href="<?= get_category_link($cat); ?>">
								<?= $term->name; ?>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
				<div class="cats-wrap">
					<?php $cats = [wp_get_object_terms($args['post']->ID, 'department', ['fields' => 'ids']),
						wp_get_object_terms($args['post']->ID, 'place_type', ['fields' => 'ids'])];
					foreach ($cats as $post_terms_all) :
						foreach ($post_terms_all as $cat) : $term = get_term($cat); ?>
							<a class="post-info" href="<?= get_category_link($cat); ?>">
								<?= $term->name; ?>
							</a>
						<?php endforeach;
					endforeach; ?>
				</div>
			<p class="post-preview mb-3">
				<?php $preview = get_field('home_desc', $args['post']);
				echo $preview ? text_preview($preview, 10) : text_preview($args['post']->post_content, 10); ?>
			</p>
			<a href="<?php the_permalink($args['post']); ?>" class="base-link">
				לבית האבות
			</a>
		</div>
	</div>
<?php endif; ?>
