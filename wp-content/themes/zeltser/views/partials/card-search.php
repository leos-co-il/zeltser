<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-12 post-item-col search-item-col">
		<div class="row more-card search-card" data-id="<?= $args['post']->ID; ?>">
			<a class="col-md-4 post-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
			   href="<?= $link; ?>"></a>
			<div class="col-md-8 post-item-content">
				<h3 class="post-item-title"><?= $args['post']->post_title; ?></h3>
				<h3 class="post-info font-weight-bold">
					<?= get_field('home_address', $args['post']); ?>
				</h3>
				<div class="cats-wrap">
					<?php $cats = [wp_get_object_terms($args['post']->ID, 'location', ['fields' => 'ids']),
						wp_get_object_terms($args['post']->ID, 'department', ['fields' => 'ids']),
						wp_get_object_terms($args['post']->ID, 'place_type', ['fields' => 'ids'])];
					foreach ($cats as $post_terms_all) :
						foreach ($post_terms_all as $cat) : $term = get_term($cat); ?>
							<a class="post-info" href="<?= get_category_link($cat); ?>">
								<?= $term->name; ?>
							</a>
						<?php endforeach;
					endforeach; ?>
				</div>
				<p class="post-preview mb-3">
					<?php if ($preview = get_field('home_desc', $args['post'])) {
						echo text_preview($preview, 10);
					} else {
						echo text_preview($args['post']->post_content, 10);
					}?>
				</p>
				<a href="<?php the_permalink($args['post']); ?>" class="search-post-link">
					לפרטים נוספים
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
