<div class="form-col mb-3">
	<?php if (isset($args['logo']) && $args['logo']) : ?>
		<a class="logo-form" href="/">
			<img src="<?= $args['logo']['url']; ?>" alt="logo">
		</a>
	<?php endif;
	if ($f_title = opt('mid_form_title')) : ?>
		<h2 class="form-title text-center"><?= $f_title; ?></h2>
	<?php endif;
	if ($f_text = opt('mid_form_text')) : ?>
		<h3 class="form-subtitle text-center"><?= $f_text; ?></h3>
	<?php endif;
	getForm('7'); ?>
</div>
