<?php
$cats_dep = get_terms([
	'taxonomy' => 'department',
	'hide_empty' => false,
]);
$place_types = get_terms([
	'taxonomy' => 'place_type',
	'hide_empty' => false,
]);
$locations = get_terms([
	'taxonomy' => 'location',
	'hide_empty' => false,
	'parent' => 0,
]);
$search_block_title = opt('search_block_title');
$search_block_text = opt('search_block_text');
?>

<div class="main-search-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="search-section-back">
					<?php if ($search_block_title || $search_block_text) : ?>
						<div class="row justify-content-center align-items-center mb-2">
							<?php if ($search_block_title) : ?>
								<div class="col-auto">
									<h2 class="search-title"><?= $search_block_title; ?></h2>
								</div>
							<?php endif;
							if ($search_block_text) : ?>
								<div class="col-auto">
									<h2 class="search-text"><?= $search_block_text; ?></h2>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<form method="get" action="<?= opt('search_page')['url'] ?>">
						<div class="row align-items-stretch">
							<?php if ($locations): ?>
								<div class="form-group col-xl col-lg-3 col-sm-6 col-12">
									<select id="inputLoc" name="place-location" class="form-control">
										<option selected disabled><?= esc_html__('בחרו אזור','leos')  ?></option>
										<?php foreach($locations as $location): ?>
											<option value="<?= $location->term_id ?>" data-id="<?= $location->term_id ?>"><?= $location->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-lg-3 col-sm-6 col-12">
								<select id="inputCity" name="place-city" class="form-control">
									<option selected disabled><?= esc_html__('בחרו עיר או ישוב ','leos')  ?></option>
								</select>
							</div>
							<?php if ($place_types): ?>
								<div class="form-group col-xl col-lg-3 col-sm-6 col-12">
									<select id="inputTypePlace" name="place-type" class="form-control">
										<option selected disabled><?= esc_html__('בחרו סוג מוסד','leos')  ?></option>
										<?php foreach($place_types as $place_type): ?>
											<option value="<?= $place_type->term_id ?>"><?= $place_type->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<?php if ($cats_dep): ?>
								<div class="form-group col-xl col-lg-3 col-sm-6 col-12">
									<select id="inputDepartment" name="place-department" class="form-control">
										<option selected disabled><?= esc_html__('בחרו מחלקה','leos')  ?></option>
										<?php foreach($cats_dep as $department): ?>
											<option value="<?= $department->term_id ?>" data-id="<?= $department->term_id ?>">
												<?= $department->name ?>
											</option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-12">
								<button type="submit" class="btn btn-search"><?= esc_html__('חפשו את הדיור המתאים','leos') ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
