<?php if (isset($args['banner']) && $args['banner']) : ?>
<div class="banner-block">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12">
				<img src="<?= $args['banner']['url']; ?>">
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
