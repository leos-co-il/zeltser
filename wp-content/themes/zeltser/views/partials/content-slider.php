<?php if (isset($args['content']) && $args['content']) : ?>
	<section class="slider-base-wrap arrows-slider">
		<div class="container-fluid slider-container">
			<div class="row justify-content-center align-items-center slider-row">
				<div class="col-lg-6 col-12 <?= (isset($args['img']) && $args['img']) ? '': 'col-lg-12'; ?> slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="post-text-output"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if (isset($args['img']) && $args['img']) : ?>
					<div class="col-lg-6 col-12 d-flex justify-content-center align-items-center slider-img-col">
						<img src="<?= $args['img']['url']; ?>" class="slider-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
