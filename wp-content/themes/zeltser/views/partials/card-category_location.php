<?php if(isset($args['category']) && $args['category']) :
	$link = get_term_link($args['category']); ?>
	<div class="post-item">
		<a class="cat-item-image cat-loc-img" <?php if ($img = get_field('cat_img', $args['category'])) : ?>
			style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>
		   href="<?= $link; ?>">
			<div class="cat-location">
				<?php if ($img = get_field('cat_icon', $args['category'])) : ?>
					<img src="<?= $img['url']; ?>" alt="category-icon" class="mb-3">
					<h3 class="cat-item-title"><?= $args['category']->name; ?></h3>
				<?php endif; ?>
			</div>
		</a>
	</div>
<?php endif; ?>
