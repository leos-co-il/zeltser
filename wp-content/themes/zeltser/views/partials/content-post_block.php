<?php if (isset($args['block']) && $args['block']) : ?>
	<div class="col-lg-6 col-12 col-repeat-post">
		<?php if ($args['block']['posts_block_title'] || $args['block']['posts_block_link']) : ?>
			<div class="row">
				<div class="col-12">
					<div class="title-row">
						<div class="row justify-content-between align-items-center">
							<?php if ($args['block']['posts_block_title']) : ?>
								<div class="col-auto d-flex align-items-center">
									<h3 class="subtitle-post-block">
										<?= $args['block']['posts_block_title']; ?>
									</h3>
								</div>
							<?php endif;
							if ($args['block']['posts_block_link']) : ?>
								<div class="col-auto">
									<a href="<?= $args['block']['posts_block_link']['url']; ?>" class="base-link base-link-white">
										<?= isset($args['block']['posts_block_link']['title']) ? $args['block']['posts_block_link']['title'] :
											'לעוד מאמרים'; ?>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;
		if ($args['block']['posts_block_posts']) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['block']['posts_block_posts'] as $x => $post) : ?>
					<div class="col-md-6 col-12 post-col wow fadeIn" data-wow-delay="0.<?= $x * 2; ?>s">
						<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>
