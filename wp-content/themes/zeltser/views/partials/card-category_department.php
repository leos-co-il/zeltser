<?php if(isset($args['category']) && $args['category']) :
	$link = get_term_link($args['category']); ?>
	<div class="post-item align-items-center">
		<a class="cat-item-image" <?php if ($img = get_field('cat_img', $args['category'])) : ?>
			style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>
		   href="<?= $link; ?>"></a>
		<a class="cat-item-title" href="<?= $link; ?>"><?= $args['category']->name; ?></a>
	</div>
<?php endif; ?>
