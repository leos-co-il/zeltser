<?php if (isset($args['text']) && $args['text']) : ?>
	<div class="row justify-content-center">
		<div class="col-xl-6 col-lg-8 col-md-10 col-12 mb-2">
			<div class="block-text text-center">
				<?= $args['text']; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
