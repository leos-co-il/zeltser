<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="repeat-form-wrap">
				<div class="row align-items-center justify-content-around">
					<div class="col-xl-auto col-lg-4 col-12 form-col align-items-start repeat-form-col">
						<?php if (isset($args['search_title']) && $args['search_title']) : ?>
							<h2 class="form-title"><?= $args['search_title']; ?></h2>
						<?php else: if ($f_title = opt('mid_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('mid_form_text')) : ?>
							<h3 class="form-subtitle"><?= $f_text; ?></h3>
						<?php endif;
						endif; ?>
					</div>
					<div class="col-xl-7 col-lg-8 col-12">
						<?php getForm('6'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
