<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']);
$type = (isset($args['post_type'])) ? $args['post_type'] : 'post'; ?>
	<div class="col-xl-3 col-sm-6 col-12 post-item-col ajax-post-item">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
			   href="<?= $link; ?>"></a>
			<div class="post-item-content">
				<h3 class="post-item-title"><?= $args['post']->post_title; ?></h3>
				<?php if ($type === 'post') : ?>
					<h3 class="post-info"><?php $author_id = $args['post']->post_author;
						echo get_the_author_meta('display_name', $author_id); ?></h3>
				<?php endif;
				if ($type === 'place') : ?>
					<h3 class="post-info font-weight-bold">
						<?= get_field('home_address', $args['post']); ?>
					</h3>
				<?php endif;
				if ($type === 'place') : ?>
					<div class="cats-wrap">
						<?php $cats = [wp_get_object_terms($args['post']->ID, 'location', ['fields' => 'ids']),
							wp_get_object_terms($args['post']->ID, 'department', ['fields' => 'ids']),
							wp_get_object_terms($args['post']->ID, 'place_type', ['fields' => 'ids'])];
						foreach ($cats as $post_terms_all) :
							foreach ($post_terms_all as $cat) : $term = get_term($cat); ?>
								<a class="post-info" href="<?= get_category_link($cat); ?>">
									<?= $term->name; ?>
								</a>
							<?php endforeach;
						endforeach; ?>
					</div>
				<?php endif; ?>
				<p class="post-preview mb-3">
					<?php if (($type === 'place') && ($preview = get_field('home_desc', $args['post']))) {
						echo text_preview($preview, 10);
					} else {
						echo text_preview($args['post']->post_content, 10);
					}?>
				</p>
				<a href="<?php the_permalink($args['post']); ?>" class="base-link align-self-end">
					<?= ($type === 'place') ? 'לבית האבות' : 'למאמר'; ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
