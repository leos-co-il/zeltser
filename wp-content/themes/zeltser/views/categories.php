<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats_dep = $fields['departments'] ? : get_terms([
		'taxonomy' => 'department',
		'hide_empty' => false,
]);
$locations = $fields['locations'] ? : get_terms([
		'taxonomy' => 'location',
		'hide_empty' => false,
		'parent' => 0,
]);
$homes = $fields['cats_homes_chosen'] ? : get_posts([
		'numberposts' => 10,
		'post_type' => 'place',
]);
?>

<article class="page-block">
	<?php if (has_post_thumbnail()) : ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="cat-main-back" style="background-image: url('<?= postThumb(); ?>')"></div>
				</div>
			</div>
		</div>
	<?php endif;
	get_template_part('views/partials/repeat', 'search');
	if ($cats_dep || $fields['cat_departments_text']) : ?>
		<div class="departments-block">
			<div class="container-fluid">
				<?php if ($fields['cat_departments_text']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['cat_departments_text'],
					]);
				}
				if ($cats_dep) : ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($cats_dep as $y => $cat_dep) : ?>
							<div class="col-lg-3 col-sm-6 col-12 wow zoomIn cat-col" data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'category_department', [
										'category' => $cat_dep,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif;
	if ($homes || $fields['cats_homes_text'] || $fields['cats_homes_link']) : ?>
		<div class="departments-block">
			<div class="container-fluid">
				<?php if ($fields['cats_homes_text']) {
					get_template_part('views/partials/content', 'block_text', [
							'text' => $fields['cats_homes_text'],
					]);
				}
				if ($homes) : ?>
					<div class="row justify-content-center align-items-stretch mt-2">
						<?php foreach ($homes as $y => $home) : ?>
							<div class="col-lg-4 col-12 wow zoomIn cat-col home-cat-col"
								 data-wow-delay="0.<?= $y + 2; ?>s">
								<?php get_template_part('views/partials/card', 'post_home', [
										'post' => $home,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if ($fields['cats_homes_link']) : ?>
					<div class="row justify-content-end my-2">
						<div class="col-auto">
							<a href="<?= $fields['cats_homes_link']['url']; ?>" class="base-link">
								<?= isset($fields['cats_homes_link']['title']) ? $fields['cats_homes_link']['title'] : 'לכל המומלצים'; ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
if ($fields['adv_img_1']) {
	get_template_part('views/partials/repeat', 'banner_fluid', [
			'banner' => $fields['adv_img_1'],
	]);
}
if ($locations || $fields['cat_locations_text']) : ?>
	<section class="locations-block">
		<div class="container-fluid">
			<?php if ($fields['cat_locations_text']) {
				get_template_part('views/partials/content', 'block_text', [
						'text' => $fields['cat_locations_text'],
				]);
			}
			if ($locations) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($locations as $y => $location) : ?>
						<div class="col-lg-3 col-sm-6 col-12 wow zoomIn cat-col" data-wow-delay="0.<?= $y + 2; ?>s">
							<?php get_template_part('views/partials/card', 'category_location', [
									'category' => $location,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_template_part('views/partials/repeat', 'form');
get_footer(); ?>
