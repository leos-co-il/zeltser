<?php
$names = ['facebook', 'twitter', 'instagram', 'youtube', 'linkedin'];
$socials = get_social_links($names);
$facebook = opt('facebook');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
		</a>
		<div class="container-fluid">
			<div class="row justify-content-between">
				<div class="col-xl-2 col-lg-3 col-sm-6 col-12 foo-menu main-footer-menu">
					<h3 class="foo-title">תפריט עזר</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2', '',
								'main_menu h-100 text-right'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu links-footer-menu">
					<h3 class="foo-title">ביטויים לקידום</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey four-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-sm-6 col-12 foo-menu contacts-footer-menu">
					<h3 class="foo-title">אנחנו גם כאן</h3>
					<div class="menu-border-top contact-menu-foo">
						<?php if ($facebook) : ?>
							<div class="facebook-widget">
								<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
										width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
										frameborder="0" allowTransparency="true" allow="encrypted-media">
								</iframe>
							</div>
						<?php endif;
						if ($socials) : ?>
							<div class="socials-footer">
								<?php foreach ($socials as $social_link) : if ($social_link['url']) : ?>
									<a href="<?= $social_link['url']; ?>" target="_blank" class="social-link">
										<i class="fab fa-<?php echo $social_link['name'];
										echo ($social_link['name'] === 'linkedin') ? '-in' : '';
										echo ($social_link['name'] === 'facebook') ? '-f' : '';?>">
										</i>
									</a>
								<?php endif; endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
