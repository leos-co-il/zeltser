<?php
get_header();
$query = get_queried_object();
$posts = get_posts([
	'numberposts' => 10,
	'post_type' => 'post',
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
?>
<article class="page-body">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg8 col-md-10 col-12 mb-2">
				<div class="block-text text-center">
					<h2><?= $query->name; ?></h2>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="body-output">
		<?php if ($posts) : $counter = 1; ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($posts as $x => $post) : ?>
				<div class="col-xl-3 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
					<?php get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]); ?>
				</div>
				<?php if(($counter === 2) && ($img_1 = get_field('adv_img_1', $query))) : ?>
					<div class="col-xl-6 col-12 mb-3">
						<img src="<?= $img_1['url']; ?>" class="w-100">
					</div>
				<?php endif; ?>
				<?php if(($counter % 6 === 0) || ($counter === count($posts) && $counter <= 6)) : ?>
			</div>
		</div>
		<?php if ($img_2 = get_field('adv_img_2', $query)) {
			get_template_part('views/partials/repeat', 'banner_fluid', [
				'banner' => $img_2,
			]);
		} ?>
		<div class="container-fluid pt-4">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php endif; $counter++; endforeach; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="load-more-link load-more-posts" data-term="<?= $query->term_id; ?>" data-type="post"
				data-tax-type="category">
					הצג מאמרים נוספים
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>

